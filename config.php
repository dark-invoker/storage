<?php

use Enums\AppTypesEnum;
use Enums\StorageTypesEnum;

/**
 * CONFIG
 */
return [
    /**
     * APP
     */
    'app' => [
        'type' => AppTypesEnum::CLI,
    ],
    /**
     * STORAGE
     */
    'storage' => [
        'temporary_storage' => 'storage_places/temp',
        'save_to' => 'download',
        'list' => [
            'storage1' => [
                'type' => StorageTypesEnum::FILE_STORAGE,
                'path' => 'storage_places',
                'size' => '2mb',
            ],
            'storage2' => [
                'type' => StorageTypesEnum::FILE_STORAGE,
                'path' => 'storage_places',
                'size' => '5mb',
            ],
            'storage3' => [
                'type' => StorageTypesEnum::FILE_STORAGE,
                'path' => 'storage_places',
                'size' => '1mb',
            ],
            'storage4' => [
                'type' => StorageTypesEnum::FILE_STORAGE,
                'path' => 'storage_places',
                'size' => '4mb',
            ],
//            'storage5' => [
//                'type' => StorageTypesEnum::DB_STORAGE,
//                'path' => 'test',
//                'size' => '3mb',
//            ],
        ],
    ],
];