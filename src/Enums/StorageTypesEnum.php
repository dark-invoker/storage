<?php

namespace Enums;

class StorageTypesEnum extends Enum
{
    const FILE_STORAGE = 1;

    const DB_STORAGE = 2;
}