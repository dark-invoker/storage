<?php

namespace Enums;

class CommandsListEnum
{
    /**
     * Displays a complete list of commands with a description
     * @Example: php storage help
     */
    const COMMAND_HELP = 'help';

    /**
     * Save file to storage
     * @Example: php storage save https://images.wallpaperscraft.ru/image/siluet_luna_lodka_135277_1920x1080.jpg
     */
    const COMMAND_SAVE = 'save';

    /**
     * Get file by id
     * @Example: php storage get {id}
     */
    const COMMAND_GET = 'get';
}