<?php

namespace Enums;

class AppTypesEnum extends Enum
{
    public const CLI = 1;

    public const WEB = 2;
}