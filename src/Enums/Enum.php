<?php

namespace Enums;

class Enum
{
    public const DEFAULT = 0;

    public static function getList()
    {
        return (new \ReflectionClass(static::class))
            ->getConstants();
    }
}