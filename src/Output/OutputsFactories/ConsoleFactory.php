<?php

namespace Output\OutputsFactories;

use Output\Outputs\IOutput;
use Output\Outputs\ConsoleOutput;

class ConsoleFactory implements IOutputFactory
{
    public function create(): IOutput
    {
        return new ConsoleOutput();
    }
}