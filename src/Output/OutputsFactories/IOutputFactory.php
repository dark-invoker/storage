<?php

namespace Output\OutputsFactories;

use Output\Outputs\IOutput;

interface IOutputFactory
{
    public function create(): IOutput;
}