<?php

namespace Output\Outputs;

class ConsoleOutput implements IOutput
{
    public function print(string $msg): void
    {
        fwrite(STDERR, $msg);
    }
}