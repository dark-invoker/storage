<?php

namespace Output\Outputs;

interface IOutput
{
    public function print(string $msg): void;
}