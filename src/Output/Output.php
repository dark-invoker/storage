<?php

namespace Output;

use Enums\AppTypesEnum;
use Output\Outputs\IOutput;
use Output\OutputsFactories\ConsoleFactory;
use ServiceLocator\SL;

class Output
{
    /** @var IOutput */
    protected static $console = null;

    /**
     * @param string $msg
     *
     * @throws \Exception
     */
    public static function print(string $msg): void
    {
        if (is_null(self::$console)) {
            $appType = SL::get('\Helpers\Config')->get('app')['type'];
            self::setOutputInstance($appType);
        }
        self::$console->print($msg);
    }

    /**
     * @param string $type
     *
     * @throws \Exception
     */
    private static function setOutputInstance(string $type)
    {
        switch ($type) {
            case AppTypesEnum::CLI:
                self::$console = (new ConsoleFactory())->create();
                break;
            case AppTypesEnum::WEB:
                throw new \Exception('This type of application is not implemented. Type: ' . AppTypesEnum::WEB);
                break;
            default:
                throw new \Exception('Invalid application type.');
        }
    }
}