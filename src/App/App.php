<?php

namespace App;

use App\Processor\IProcessor;

/**
 * Class App
 * @package App
 */
class App
{
    /** @var IProcessor */
    protected $processor = null;

    /**
     * App constructor.
     *
     * @param IProcessor $processor
     */
    public function __construct(IProcessor $processor)
    {
        $this->processor = $processor;
    }

    public function run(): void
    {
        $argument = $this->processor->getArgument();
        $commandName = $this->processor->getCommandName();

        try {
            $commandClass = 'App\Commands\Command' . ucfirst($commandName);
            $command = new $commandClass($argument);
        } catch (\Throwable $t) {
            throw new \Exception('This command is missing. Type: ' . $commandName);
        }

        $command->execute();
    }
}