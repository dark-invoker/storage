<?php

namespace App\Commands;

abstract class AbstractCommand
{
    /** @var string */
    protected $param = '';

    /**
     * AbstractCommand constructor.
     *
     * @param $param
     */
    public function __construct($param)
    {
        $this->param = $param;
    }

    public abstract function execute(): void;
}