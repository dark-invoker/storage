<?php

namespace App\Commands;

use Helpers\ClassDescriptions;
use Output\Output;

class CommandHelp extends AbstractCommand
{
    public function execute(): void
    {
        $outputStr = '/*' . PHP_EOL;
        $outputStr .= 'List of commands:' . PHP_EOL . PHP_EOL;

        $desc = ClassDescriptions::getConstantsDescriptions('Enums\CommandsListEnum');
        foreach ($desc as $key => $value) {
            $outputStr .= "`{$key}` -> {$value}" . PHP_EOL . PHP_EOL;
        }
        $outputStr .= '*/';

        Output::print($outputStr);
    }
}