<?php

namespace App\Commands;

use Collector\Collector;
use Output\Output;
use ServiceLocator\SL;

class CommandGet extends AbstractCommand
{
    public function execute(): void
    {
        $fileId = intval($this->param);

        $config = SL::get('\Helpers\Config');
        $saveTo = ROOT . '/' . $config->get('storage')['save_to'];

        $collector = new Collector();

        $filePath = $collector->collect($saveTo, $fileId);

        Output::print('Your file: ' . $filePath);
    }
}