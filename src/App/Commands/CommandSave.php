<?php

namespace App\Commands;

use Helpers\Byte;
use Helpers\PathBuilder;
use Keep\Keeper;
use Models\FileModel;
use Output\Output;
use ServiceLocator\SL;
use Storage\File\File;
use Storage\Storage;
use Storage\StorageInfo;
use Storage\StorageHelper;

class CommandSave extends AbstractCommand
{
    public function execute(): void
    {
        // create storage if not exist
        StorageHelper::createStoragesIfNotExist();

        // save to temp
        $config = SL::get('\Helpers\Config');
        $pathBuilder = new PathBuilder();
        $path = $this->param;

        $tempStorage = ROOT . '/' . $config->get('storage')['temporary_storage'] . '/';

        $pathBuilder->setBasePath($tempStorage);
        $extension = @pathinfo($path)['extension'];
        $pathBuilder->setPath(uniqid(uniqid()) . '.' . $extension);

        $keeper = new Keeper();
        $keeper->save($path, $pathBuilder->getFullPath());

        // get info for writes
        $fileSize = filesize($pathBuilder->getFullPath());
        $storagesData = StorageInfo::getStoragesDataWithFreeSizes();
        $isEnough = StorageInfo::enoughStorageSpace($storagesData, $fileSize);

        if (!$isEnough) {
            unlink($pathBuilder->getFullPath());
            throw new \Exception('Not enough storage space.');
        }

        $optimalSetOfStorages = StorageInfo::getOptimalSetOfStorages($storagesData, $fileSize);

        // save file name to db
        $now = date('Y-m-d H:i:s');
        $fileId = FileModel::insertGetId([
            'name' => $pathBuilder->getPath(),
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        $byteReader = new Byte($pathBuilder->getFullPath(), 'r');
        $file = new File();

        foreach ($optimalSetOfStorages as $storageName => &$optimalSetOfStorage) {
            $storageObj = new Storage($optimalSetOfStorage['type']);

            $file->setId($fileId);
            $file->setName($pathBuilder->getPath());
            $file->setContent($byteReader->read($optimalSetOfStorage['freeSize']));
            $file->setContentLength($optimalSetOfStorage['freeSize']);
            $file->setStoragePath($optimalSetOfStorage['path']);

            $storageObj->setFile($file);
            $storageObj->save();
        }

        $byteReader->close();
        unlink($pathBuilder->getFullPath());

        Output::print("id: {$fileId}");
    }
}