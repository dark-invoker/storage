<?php

namespace App\Processor;

class Processor implements IProcessor
{
    /** @var string */
    private $commandName = '';

    /** @var string */
    private $argument = '';

    public function __construct($params)
    {
        $this->checkArguments($params);
        $this->commandName = $params[1] ?? '';
        $this->argument = $params[2] ?? '';
    }

    /**
     * @param array $params
     *
     * @return int
     */
    public function checkArguments(array $params): int
    {
        if (sizeof($params) != 3 && $params[1] != 'help') {
            $msg = 'Parameter must consist of two arguments. Example: php storage {command} {input}';
            throw new \InvalidArgumentException($msg);
        }

        return 0;
    }

    /**
     * @return string
     */
    public function getCommandName(): string
    {
        return $this->commandName;
    }

    /**
     * @return string
     */
    public function getArgument(): string
    {
        return $this->argument;
    }
}