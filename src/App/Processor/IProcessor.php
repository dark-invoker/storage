<?php

namespace App\Processor;

interface IProcessor
{
    public function getCommandName(): string;

    public function getArgument(): string;
}