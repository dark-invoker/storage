<?php

namespace Storage;

use Enums\StorageTypesEnum;
use Helpers\Folder;
use ServiceLocator\SL;

class StorageInfo
{
    /**
     * Get in the form of an array of type: ['key' => 'value']
     * where the key is the storage path and the value is free space
     * @return array
     */
    public static function getStoragesDataWithFreeSizes(): array
    {
        $config = SL::get('\Helpers\Config');
        $storagesSettings = $config->get('storage')['list'];

        $storagesData = [];
        foreach ($storagesSettings as $storageName => &$setting) {
            if ($setting['type'] == StorageTypesEnum::DB_STORAGE) {
                continue;
            }

            $storageFullPath = ROOT . "/{$setting['path']}/{$storageName}";

            $dirSize = Folder::getSize($storageFullPath);
            $sizeInBytes = intval($setting['size']) * pow(1024, 2);
            $freeSize = $sizeInBytes - $dirSize;

            if ($freeSize > 0) {
                $storagesData[$storageName] = [
                    'path' => "{$setting['path']}/{$storageName}",
                    'fullPath' => $storageFullPath,
                    'freeSize' => $freeSize,
                    'type' => $setting['type'],
                ];
            }
        }

        return $storagesData;
    }

    /**
     * Checks if there is enough storage space
     *
     * @param array $storagesData
     * @param int $fileSize
     *
     * @return bool
     */
    public static function enoughStorageSpace(array $storagesData, int $fileSize): bool
    {
        $fOk = false;
        $freeSize = array_sum(array_column($storagesData, 'freeSize'));

        if ($freeSize >= $fileSize) {
            $fOk = true;
        }

        return $fOk;
    }

    /**
     * Get the optimal set of storages for writing a file
     *
     * @param array $storagesData
     * @param int $fileSize
     *
     * @return array
     */
    public static function getOptimalSetOfStorages(array $storagesData, int $fileSize)
    {
        usort($storagesData, function ($current, $next) {
            return ($next['freeSize'] - $current['freeSize']);
        });

        $optimalSetOfStorages = [];
        foreach ($storagesData as $storageName => &$storageSetting) {
            if ($fileSize <= 0) {
                break;
            }

            $optimalSetOfStorages[$storageName] = [
                'fullPath' => $storageSetting['fullPath'],
                'freeSize' => $storageSetting['freeSize'],
                'path' => $storageSetting['path'],
                'type' => $storageSetting['type'],
            ];
            $fileSize -= $storageSetting['freeSize'];
        }

        return $optimalSetOfStorages;
    }
}