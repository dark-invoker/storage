<?php

namespace Storage\Storages;

use Helpers\Byte;
use Models\StorageModel;
use Models\StorageFileModel;

class FileStorage extends AbstractStorage
{
    public function save()
    {
        $saveTo = ROOT . "/{$this->file->getStoragePath()}/{$this->file->getName()}";

        // write to storage
        $byteWriter = new Byte($saveTo, 'w+');

        $byteWriter->write(
            $this->file->getContent(),
            $this->file->getContentLength()
        );

        $byteWriter->close();

        $storageModel = StorageModel::wherePath($this->file->getStoragePath())->first();

        if (!is_null($storageModel)) {
            StorageFileModel::insert([
                'file_id' => $this->file->getId(),
                'storage_id' => $storageModel->id,
            ]);
        }
    }
}