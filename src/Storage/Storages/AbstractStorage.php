<?php

namespace Storage\Storages;

use Storage\File\IFile;

abstract class AbstractStorage
{
    /** @var IFile */
    protected $file;

    /**
     * FileStorage constructor.
     *
     * @param IFile $file
     */
    public function __construct(IFile $file = null)
    {
        $this->file = $file;
    }

    public abstract function save();
}