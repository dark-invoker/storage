<?php

namespace Storage;

use Enums\StorageTypesEnum;
use Storage\File\IFile;
use Storage\StorageFactories\DatabaseStorageFactory;
use Storage\StorageFactories\FileStorageFactory;

/**
 * Class Storage
 * @package Storage
 */
class Storage
{
    /** @var IStorage */
    protected $storageInstance;

    /** @var int */
    protected $storageType;

    /** @var IFile */
    protected $file;

    /**
     * @return void
     * @throws \Exception
     */
    public function save(): void
    {
        if (is_null($this->storageInstance)) {
            $this->setStorageInstance();
        }

        $this->storageInstance->save();
    }

    /**
     * @param IFile $file
     */
    public function setFile(IFile $file)
    {
        $this->file = $file;
    }

    /**
     * AbstractStorage constructor.
     *
     * @param int $storageType
     *
     * @example $storageType = StorageTypesEnum::FILE_STORAGE
     */
    public function __construct(int $storageType)
    {
        $this->storageType = $storageType;
    }

    /**
     * @throws \Exception
     */
    protected function setStorageInstance()
    {
        switch ($this->storageType) {
            case StorageTypesEnum::FILE_STORAGE:
                $this->storageInstance = (new FileStorageFactory($this->file))->create();
                break;
            case StorageTypesEnum::DB_STORAGE:
                $this->storageInstance = (new DatabaseStorageFactory($this->file))->create();
                break;
            default:
                throw new \Exception('This type of storage is missing. Type: ' . $this->storageType);
                break;
        }
    }
}