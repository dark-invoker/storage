<?php

namespace Storage;

use Enums\StorageTypesEnum;
use Helpers\Folder;
use Models\StorageModel;
use ServiceLocator\SL;

class StorageHelper
{
    public static function createStoragesIfNotExist(): void
    {
        $config = SL::get('\Helpers\Config');
        $storagesSettings = $config->get('storage')['list'];

        $downloadFolder = $config->get('storage')['save_to'];
        Folder::createIfNotExist(ROOT . '/' . $downloadFolder);

        $bulkInsert = [];
        $now = date('Y-m-d H:i:s');

        foreach ($storagesSettings as $storageName => &$storageSetting) {
            if ($storageSetting['type'] == StorageTypesEnum::DB_STORAGE) {
                continue;
            }

            Folder::createIfNotExist(ROOT . '/' . $storageSetting['path']);
            $created = Folder::createIfNotExist(ROOT . '/' . $storageSetting['path'] . '/' . $storageName);

            if ($created) {
                $bulkInsert[] = [
                    'path' => $storageSetting['path'] . '/' . $storageName,
                    'type' => $storageSetting['type'],
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
            }
        }
        //add to db
        if (!empty($bulkInsert)) {
            StorageModel::insert($bulkInsert);
        }

        $tempStorage = ROOT . '/' . $config->get('storage')['temporary_storage'];
        Folder::createIfNotExist($tempStorage);
    }
}