<?php

namespace Storage\StorageFactories;

use Storage\File\IFile;
use Storage\Storages\AbstractStorage;

abstract class AbstractStorageFactory
{
    /** @var IFile */
    protected $file;

    public function __construct(IFile $file = null)
    {
        $this->file = $file;
    }

    abstract public function create(): AbstractStorage;
}