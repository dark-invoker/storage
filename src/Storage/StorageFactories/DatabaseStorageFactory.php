<?php

namespace Storage\StorageFactories;

use Storage\Storages\AbstractStorage;
use Storage\Storages\DatabaseStorage;

class DatabaseStorageFactory extends AbstractStorageFactory
{
    public function create(): AbstractStorage
    {
        return new DatabaseStorage($this->file);
    }
}