<?php

namespace Storage\StorageFactories;

use Storage\Storages\AbstractStorage;
use Storage\Storages\FileStorage;

class FileStorageFactory extends AbstractStorageFactory
{
    public function create(): AbstractStorage
    {
        return new FileStorage($this->file);
    }
}