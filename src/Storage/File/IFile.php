<?php

namespace Storage\File;

interface IFile
{
    public function getId();

    public function getContent();

    public function getContentLength();

    public function getStoragePath();

    public function getName();
}