<?php

namespace Helpers;

use ServiceLocator\IService;

class Config implements IService
{
    /** @var array */
    protected $config = null;

    public function __construct()
    {
        $this->config = include(ROOT . '/config.php');
    }

    /**
     * @param string $key
     *
     * @return mixed
     * @throws \Exception
     */
    public function get(string $key)
    {
        if (!is_null($this->config)) {
            return $this->config[$key];
        }

        throw new \Exception('Сonfig is empty.');
    }
}