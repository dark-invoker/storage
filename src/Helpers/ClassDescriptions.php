<?php

namespace Helpers;

class ClassDescriptions
{
    /**
     * @param string $className
     *
     * @return array
     * @throws \ReflectionException
     */
    public static function getConstantsDescriptions(string $className): array
    {
        $docs = [];
        $consts = (new \ReflectionClass($className))->getConstants();

        foreach ($consts as $constName => $constValue) {
            $rfClConsts = new \ReflectionClassConstant($className, $constName);

            $cleanInfo = strtr($rfClConsts->getDocComment(), [
                '/**' => '',
                '*/' => '',
                '*' => '',
            ]);

            $docs[$constValue] = trim($cleanInfo);
        }

        return $docs;
    }
}