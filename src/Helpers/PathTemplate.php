<?php

namespace Helpers;

class PathTemplate
{
    /** @var string */
    protected $linkPattern = '/^(https?:\/\/)?(.+)\.([a-z]{2,6}\.?)(\/)/';

    /**
     * @param string $pattern
     * @param string $str
     *
     * @return bool
     */
    protected function checkPattern(string $pattern, string $str): bool
    {
        $fOk = false;

        if (preg_match($pattern, $str)) {
            $fOk = true;
        }

        return $fOk;
    }

    /**
     * @param string $str
     *
     * @return bool
     */
    public function isLink(string $str): bool
    {
        return $this->checkPattern($this->linkPattern, $str);
    }

    /**
     * @param string $str
     *
     * @return bool
     */
    public function isFilePath(string $str): bool
    {
        return file_exists($str) && !is_dir($str);
    }
}