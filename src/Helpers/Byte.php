<?php

namespace Helpers;

class Byte
{
    protected $handle;

    protected $mode;

    protected $filePath;

    /**
     * Byte constructor.
     *
     * @param string $filePath
     * @param string $mode
     */
    public function __construct(string $filePath, string $mode = 'r+')
    {
        $this->mode = $mode;
        $this->filePath = $filePath;
        $this->handle = fopen($this->filePath, $this->mode);
    }

    /**
     * @param int $quantity
     *
     * @return false|string
     */
    public function read(int $quantity)
    {
        return fread($this->handle, $quantity);
    }

    /**
     * @param $content
     * @param int $quantity
     *
     * @return false|int
     */
    public function write($content, int $quantity)
    {
        return fwrite($this->handle, $content, $quantity);
    }

    public function close()
    {
        fclose($this->handle);
    }
}