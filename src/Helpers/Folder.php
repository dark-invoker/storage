<?php

namespace Helpers;

class Folder
{
    /**
     * Create folder
     *
     * @param string $path
     * @param int $mode
     *
     * @return bool
     */
    public static function createIfNotExist(string $path, $mode = 0777): bool
    {
        $fOk = false;

        if (!file_exists($path)) {
            mkdir($path, $mode);
            $fOk = true;
        }

        return $fOk;
    }

    /**
     * Returns the size of the folder in bytes
     *
     * @param string $path
     *
     * @return int
     */
    public static function getSize(string $path): int
    {
        $iter = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));

        $size = 0;
        foreach ($iter as $file) {
            $size += $file->getSize();
        }

        return $size;
    }

    /**
     * Get a list of folder names inside a folder by path $path
     *
     * @param string $path
     * @param array $except , - list of names to exclude
     *
     * @return array
     */
    public static function getNames(string $path, array $except = [])
    {
        return array_diff(scandir($path), array_merge($except, ['.', '..']));
    }
}