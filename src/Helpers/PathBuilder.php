<?php

namespace Helpers;

class PathBuilder
{
    /** @var string */
    protected $basePath = '';

    /** @var string */
    protected $path = '';

    /**
     * @param string $basePath
     */
    public function setBasePath(string $basePath): void
    {
        $this->basePath = $basePath;
    }

    /**
     * @return string
     */
    public function getBasePath(): string
    {
        return $this->basePath;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getFullPath(): string
    {
        return "{$this->basePath}{$this->path}";
    }

    /**
     * @return void
     */
    public function clearPath(): void
    {
        $this->path = '';
    }

    /**
     * @return void
     */
    public function clearBasePath(): void
    {
        $this->basePath = '';
    }

    /**
     * @return void
     */
    public function clear(): void
    {
        $this->clearPath();
        $this->clearBasePath();
    }
}