<?php

namespace ServiceLocator;

/**
 * Class SL
 * @package ServiceLocator
 */
class SL implements IServiceLocator
{
    /** @var array */
    private static $params = [];

    /** @var Service[] */
    private static $services = [];

    /**
     * Check if there is a service
     *
     * @param string $interface
     *
     * @return bool
     */
    public static function has(string $interface): bool
    {
        return isset(self::$params[$interface]) || isset(self::$services[$interface]);
    }

    /**
     * Register service
     *
     * @param string $class
     * @param array $params
     */
    public static function register(string $class, array $params = []): void
    {
        self::$params[$class] = $params;
    }

    /**
     * Get instance of IService
     *
     * @param string $class
     *
     * @return IService
     */
    public static function get(string $class): IService
    {
        if (isset(self::$services[$class])) {
            return self::$services[$class];
        }

        $obj = new $class(...self::$params[$class]);

        if (!$obj instanceof IService) {
            throw new InvalidArgumentException('Could not register service: is no instance of IService');
        }

        return (self::$services[$class] = $obj);
    }
}