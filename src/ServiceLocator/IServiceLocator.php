<?php

namespace ServiceLocator;

interface IServiceLocator
{
    public static function register(string $class, array $params): void;

    public static function get(string $class): IService;

    public static function has(string $interface): bool;
}