<?php

namespace Collector\Collectors;

interface ICollector
{
    public function collect($saveTo, $param): string;
}