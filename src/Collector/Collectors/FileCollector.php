<?php

namespace Collector\Collectors;

class FileCollector implements ICollector
{
    public function collect($pathTo, $pathFrom): string
    {
        $saveTo = $pathTo . '/' . basename($pathFrom);

        file_put_contents(
            $saveTo,
            file_get_contents($pathFrom),
            FILE_APPEND
        );

        return $saveTo;
    }
}