<?php

namespace Collector;

use Collector\Collectors\ICollector;
use Enums\StorageTypesEnum;
use Models\StorageFileModel;
use Collector\CollectorFactories\FileCollectorFactory;
use Collector\CollectorFactories\DBCollectorFactory;

class Collector
{
    /** @var ICollector */
    protected $instance;

    /** @var string */
    protected $storageType;

    /**
     * @param $pathTo
     * @param $param
     *
     * @return string
     * @throws \Exception
     */
    public function collect($pathTo, $param)
    {
        $fileParts = StorageFileModel::getByFileId($param)
            ->withPath()
            ->addSelect('t_storages.type AS storage_type', 't_files.name AS file_name')
            ->get();

        $fileOutput = '';
        foreach ($fileParts as &$filePart) {
            $this->setInstance($filePart->storage_type);
            $fileOutput = $this->instance->collect($pathTo, ROOT . '/' . $filePart->path);
        }

        return $fileOutput;
    }

    /**
     * @param int $storageType
     *
     * @throws \Exception
     */
    protected function setInstance(int $storageType)
    {
        switch ($storageType) {
            case StorageTypesEnum::FILE_STORAGE:
                $this->instance = (new FileCollectorFactory())->create();
                break;
            case StorageTypesEnum::DB_STORAGE:
                $this->instance = (new DBCollectorFactory())->create();
                break;
            default:
                throw new \Exception('This type of storage is missing. Type: ' . $this->storageType);
                break;
        }
    }
}