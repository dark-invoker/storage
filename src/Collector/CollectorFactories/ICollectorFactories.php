<?php

namespace Collector\CollectorFactories;

use Collector\Collectors\ICollector;

interface ICollectorFactories
{
    public function create(): ICollector;
}