<?php

namespace Collector\CollectorFactories;

use Collector\Collectors\FileCollector;
use Collector\Collectors\ICollector;

class FileCollectorFactory implements ICollectorFactories
{
    public function create(): ICollector
    {
        return new FileCollector();
    }
}