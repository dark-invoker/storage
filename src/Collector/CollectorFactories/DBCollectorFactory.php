<?php

namespace Collector\CollectorFactories;

use Collector\Collectors\DBCollector;
use Collector\Collectors\ICollector;

class DBCollectorFactory implements ICollectorFactories
{
    public function create(): ICollector
    {
        return new DBCollector();
    }
}