<?php

namespace Models;

use \Illuminate\Database\Eloquent\Model;

class StorageModel extends Model
{
    /** @var string */
    protected $primaryKey = 'id';

    /** @var string */
    protected $table = 't_storages';

    /** @var bool */
    public $timestamps = true;
}