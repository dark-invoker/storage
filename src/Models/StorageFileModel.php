<?php

namespace Models;

use \Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Capsule\Manager as DB;

class StorageFileModel extends Model
{
    /** @var string */
    protected $table = 't_storages_files';

    /**
     * @param $data
     *
     * @return void
     * @throws \Exception
     */
    public static function bulkInsert(array $data): void
    {
        if (!empty($data)) {
            DB::table('t_storages_files')->insert($data);
        } else {
            throw new \Exception('No data.');
        }
    }

    /**
     * Adds the path to the query
     *
     * @param $q
     *
     * @return mixed
     */
    public function scopeWithPath($q)
    {
        $columns = [
            DB::raw("CONCAT(t_storages.path, '/', t_files.name) AS path"),
        ];

        return $q
            ->select($columns)
            ->join('t_files', 't_files.id', '=', $this->table . '.file_id')
            ->join('t_storages', 't_storages.id', '=', $this->table . '.storage_id');
    }

    /**
     * Restrict query by file id
     *
     * @param $query
     * @param int $fileId
     *
     * @return mixed
     */
    public function scopeGetByFileId($query, int $fileId)
    {
        return $query->from($this->table)->where($this->table . '.file_id', $fileId);
    }
}