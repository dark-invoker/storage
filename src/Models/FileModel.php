<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class FileModel extends Model
{
    /** @var string */
    protected $primaryKey = 'id';

    /** @var string */
    protected $table = 't_files';

    /** @var bool */
    public $timestamps = true;
}