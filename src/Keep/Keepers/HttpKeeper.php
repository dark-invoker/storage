<?php

namespace Keep\Keepers;

use GuzzleHttp\Client;

class HttpKeeper implements IKeeper
{
    public function save(string $uri, string $saveTo): void
    {
        $http = new Client();

        $options = [
            'verify' => false,
            'save_to' => fopen($saveTo, 'w+'),
        ];

        $response = $http->get($uri, $options);

        if ($response == false) {
            throw new \Exception('Failed to save file.');
        }
    }
}