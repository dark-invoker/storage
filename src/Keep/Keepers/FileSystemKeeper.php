<?php

namespace Keep\Keepers;

class FileSystemKeeper implements IKeeper
{
    public function save(string $path, string $saveTo): void
    {
        if (!copy($path, $saveTo)) {
            throw new \Exception('Failed to copy file.');
        }
    }
}