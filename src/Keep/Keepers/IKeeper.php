<?php

namespace Keep\Keepers;

interface IKeeper
{
    public function save(string $path, string $saveTo): void;
}