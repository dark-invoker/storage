<?php

namespace Keep\KeeperFactories;

use Keep\Keepers\FileSystemKeeper;
use Keep\Keepers\IKeeper;

class FileSystemKeeperFactory implements IKeeperFactory
{
    public function create(): IKeeper
    {
        return new FileSystemKeeper();
    }
}