<?php

namespace Keep\KeeperFactories;

use Keep\Keepers\HttpKeeper;
use Keep\Keepers\IKeeper;

class HttpKeeperFactory implements IKeeperFactory
{
    public function create(): IKeeper
    {
        return new HttpKeeper();
    }
}