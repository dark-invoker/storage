<?php

namespace Keep\KeeperFactories;

use Keep\Keepers\IKeeper;

interface IKeeperFactory
{
    public function create(): IKeeper;
}