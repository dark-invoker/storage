<?php

namespace Keep;

use Keep\KeeperFactories\FileSystemKeeperFactory;
use Keep\KeeperFactories\HttpKeeperFactory;
use Helpers\PathTemplate;

class Keeper
{
    /** @var IKeeper */
    protected $instance;

    /**
     * @param string $path
     * @param string $saveTo
     *
     * @throws \Exception
     */
    public function save(string $path, string $saveTo): void
    {
        $template = new PathTemplate();

        switch (true) {
            case $template->isLink($path):
                $this->instance = (new HttpKeeperFactory())->create();
                break;
            case $template->isFilePath($path):
                $this->instance = (new FileSystemKeeperFactory())->create();
                break;
            default:
                throw new \Exception('Incorrect path. Path: ' . $path);
        }

        $this->instance->save($path, $saveTo);
    }
}