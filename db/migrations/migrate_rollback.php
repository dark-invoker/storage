<?php
/** AUTOLOAD */
require_once(dirname(dirname(__DIR__)) . '/vendor/autoload.php');

/** BOOTSTRAP */
require_once(dirname(dirname(__DIR__)) . '/bootstrap/bootstrap.php');

use Illuminate\Database\Capsule\Manager AS Database;
use Output\Output;

Output::print('drop if exist last migrations...' . PHP_EOL);


Database::statement('SET FOREIGN_KEY_CHECKS=0;');

Database::schema()->dropIfExists('t_files');
Database::schema()->dropIfExists('t_storages');
Database::schema()->dropIfExists('t_storages_files');

Database::statement('SET FOREIGN_KEY_CHECKS=1;');


Output::print('dropped' . PHP_EOL);