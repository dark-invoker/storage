<?php
/** AUTOLOAD */
require_once(dirname(dirname(__DIR__)) . '/vendor/autoload.php');

/** BOOTSTRAP */
require_once(dirname(dirname(__DIR__)) . '/bootstrap/bootstrap.php');

use Illuminate\Database\Capsule\Manager AS Database;
use \Illuminate\Database\Schema\Blueprint;
use Output\Output;


Output::print('migrating...' . PHP_EOL);

Database::schema()->create('t_files',
    function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('name', 255);
        $table->timestamps();
    });

Database::schema()->create('t_storages',
    function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('path', 255);
        $table->smallInteger('type');
        $table->timestamps();
    });

Database::schema()->create('t_storages_files',
    function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->unsignedBigInteger('storage_id');
        $table->unsignedBigInteger('file_id');
        $table->index(['file_id']);
    });

Database::schema()->table('t_storages_files',
    function (Blueprint $table) {
        $table
            ->foreign('storage_id')
            ->references('id')
            ->on('t_storages');

        $table
            ->foreign('file_id')
            ->references('id')
            ->on('t_files');
    });


Output::print('migrated' . PHP_EOL);