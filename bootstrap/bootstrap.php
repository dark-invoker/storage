<?php
/** CONSTANTS */
require_once(__DIR__ . '/src/const.php');

/** LOADING ENVIRONMENT VARIABLES */
Dotenv\Dotenv::createImmutable(ROOT)->load();

/** SET INI */
require_once(ROOT . '/bootstrap/src/ini.php');

/** LOG */
require_once(ROOT . '/bootstrap/src/log.php');

/** DB */
require_once(ROOT . '/bootstrap/src/db.php');

/** SERVICE REGISTRATION */
require_once(ROOT . '/bootstrap/src/service_registration.php');