<?php

if (getenv('DEBUG_MODE') == 'true') {
    ini_set('display_errors', 'On');
    ini_set('display_startup_errors', 'On');
    ini_set('error_reporting', E_ALL);
} else {
    ini_set('display_errors', 'Off');
}