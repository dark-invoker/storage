<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$log = new Logger('exception');

$stream = new StreamHandler(ROOT . '/logs/error_' . date('Y-m-d') . '.log.json');
$stream->setFormatter(new \Monolog\Formatter\JsonFormatter());

$log->pushHandler($stream);